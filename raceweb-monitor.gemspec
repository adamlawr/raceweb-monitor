lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "raceweb/monitor/version"

Gem::Specification.new do |spec|
  spec.name          = "raceweb-monitor"
  spec.version       = Raceweb::Monitor::VERSION
  spec.authors       = ["Adam Lawrence"]
  spec.email         = ["adam@raceweb.ca"]

  spec.summary       = %q{monitor raceweb apps.}
  spec.description   = %q{monitor raceweb apps.}
  spec.homepage      = 'https://gitlab.com/adamlawr/raceweb-monitor'
  spec.license       = 'MIT'

  # spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/adamlawr/raceweb-monitor'
  # spec.metadata['changelog_uri'] = 'TODO'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  # spec.executables   = spec.files.grep(%r{^sh/}) { |f| File.basename(f) }
  spec.executables   = ['raceweb-monitor']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_dependency             'pry', '~> 0.11.3'
  spec.add_dependency             'pry-byebug', '~> 3.6', '>= 3.6.0'
  spec.add_dependency             'twilio-ruby' # sms sending
  spec.add_dependency             'rufus-scheduler' # scheduling
  spec.add_runtime_dependency     'tzinfo-data'
end
