# Raceweb::Monitor

monitor the deployed raceweb apps. configure with env variables, override with cli options. run in a docker container.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'raceweb-monitor'
```

And then execute:

    $ bundle

## Usage

### via docker

- apps: wisdom, brainpad, portfolio - all local
- verbose: sms_full (sms success and failures)
- tag: 20200408
- via: docker

    docker run -d -e RW_MONITOR_APPS="wisdom#http://localhost:3001#avail brainpad#https://localhost:3002#avail,db_count howdido#http://localhost:3003#avail portfolio#http://localhost:3011#avail" -e RW_MONITOR_TWIL="<sid>#<token>#+<from>#+1250<to>" -e RW_MONITOR_VERBOSE=sms_full --name monitor registry.gitlab.com/adamlawr/raceweb-monitor:20210223

- apps: brainpad
- verbose: sms_full (sms success and failures)
- tag: 20200408
- via: docker

    docker run -d -e RW_MONITOR_APPS="brainpad#https://brainpad.raceweb.ca#avail,db_count" -e RW_MONITOR_TWIL="<sid>#<token>#+<from>#+1250<to>" -e RW_MONITOR_VERBOSE=sms_full --name monitor registry.gitlab.com/adamlawr/raceweb-monitor:20210223

- apps: brainpad, wisdom, portfolio, zendogz
- verbose: off (logging only)
- tag: 20200408
- via: docker

    docker run -d -e RW_MONITOR_APPS="brainpad#https://brainpad.raceweb.ca#avail,db_count wisdom#https://wisdom.raceweb.ca#avail portfolio#http://portfolio.raceweb.ca#avail zendogz#https://www.zendogz.ca/home#avail " -e RW_MONITOR_TWIL="<sid>#<token>#+<from>#+1250<to>" -e RW_MONITOR_VERBOSE=off --name monitor registry.gitlab.com/adamlawr/raceweb-monitor:20210223

- apps: brainpad
- verbose: not specified [defaults to sms_error] log both, sms only errors
- via: docker container with cli config

    docker run -d --name monitor registry.gitlab.com/adamlawr/raceweb-monitor:20210223 \
    bundle exec raceweb-monitor -v -a "brainpad#https://brainpad.raceweb.ca#avail,db_count" \
    -t "<sid>#<token>#<from>#<to>" -s "*/5 * * * *"


### via direct

- apps: wisdom
- verbose: not specified [defaults to sms_error] log both, sms only errors
- via: cli with cli options

    bundle exec raceweb-monitor -s "*/1 * * * *" -a "wisdom#http://localhost:3001#avail" -t "<sid>#<token>#+<from>#+1250<to>"

- apps: brainpad
- verbose: sms_full (sms success and failures)
- via: cli with env options

    export RW_MONITOR_APPS="brainpad#https://brainpad.raceweb.ca#avail,db_count"
    export RW_MONITOR_TWIL="<sid>#<token>#+<from>#+1250<to>"
    export RW_MONITOR_SCHEDULE="*/5 * * * *"
    export RW_MONITOR_VERBOSE=sms_full
    bundle exec raceweb-monitor

## Development

### push docker image to brainpad
use timestamp (like 20210223) as the image tag

    cd docker
    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/adamlawr/raceweb-monitor:<tag> .
    docker push registry.gitlab.com/adamlawr/raceweb-monitor:<tag>

## Links

- [rufus-scheduler gem](https://github.com/jmettraux/rufus-scheduler)

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
