require 'optparse'
require 'yaml'
require 'pry'
require 'raceweb/monitor/checker'
require 'raceweb/utils'
require 'rufus-scheduler'
require 'twilio-ruby'
require 'logger'

module Raceweb
  module Monitor
    class Cli

      SUCCESS = "\u{2705}"
      FAIL = "\u{1F4A9}"

      def initialize
        # start with default options
        @options = {
          schedule: '0 * * * *', # every hour
          verbose: 'sms_error'   # sms errors only
        }
        # merge the env options
        @options.merge!(env_options)
        # merge the cli options
        @options.merge!(cli_options)
        @options = Raceweb::Utils.symbolize(@options).freeze

        # keep 5 1MB log files
        @log = Logger.new('raceweb-monitor.log', 5, 1024000)
        @log.formatter = proc { |severity, datetime, progname, msg| "#{msg}\n" }

        if %w(sms_error sms_full).include? @options[:verbose]
          @twilio = Twilio::REST::Client.new(@options[:twilio][:sid], @options[:twilio][:token])
        end
      end

      def start
        puts 'starting with options:'
        pp @options
        puts "root: #{Dir.pwd}"
        puts "extra params: #{ARGV}"

        if @options.key? :pretend
          puts 'just pretending. see ya.'
          return
        end

        rm = Raceweb::Monitor::Checker.new
        scheduler = Rufus::Scheduler.new

        schedule = @options[:schedule]
        scheduler.cron schedule do
          # do all the things!
          @log.info "\n#{Time.now.strftime('%Y-%m-%d %H:%M:%S')} - do all the things!"
          @options[:apps].each do |app|
            # puts "check #{app[0]} #{app[1][:url]} #{app[1][:actions]}}"
            notify(app[0], rm.available?(app[1][:url])) if app[1][:actions].include?('avail')
            notify(app[0], rm.db_count?(app[1][:url])) if app[1][:actions].include?('db_count')
          end
        end
        scheduler.join
      end

      def env_options
        env_options = {}
        env_options[:apps] = parse_app_configs(ENV['RW_MONITOR_APPS'] || '')
        env_options[:twilio] = parse_twilio_config(ENV['RW_MONITOR_TWIL'] || '')
        env_options[:schedule] = ENV['RW_MONITOR_SCHEDULE'] if ENV['RW_MONITOR_SCHEDULE']
        env_options[:verbose] = ENV['RW_MONITOR_VERBOSE'] if ENV['RW_MONITOR_VERBOSE']
        env_options
      end

      def parse_app_configs(config_str = '')
        app_options = {}
        return app_options if config_str.empty?
        apps = config_str.split
        return app_options if apps.empty?
        apps.each do |app|
          app_config = app.split('#')
          app_options[app_config[0].to_sym] = {
            url: app_config[1],
            actions: app_config[2].split(',')
          }
        end
        app_options
      end

      def parse_twilio_config(config_str = '')
        twilio_options = {}
        return twilio_options if config_str.empty?
        twilio_config = config_str.split('#')
        return twilio_options if twilio_config.size < 4
        twilio_options[:sid] = twilio_config[0]
        twilio_options[:token] = twilio_config[1]
        twilio_options[:from] = twilio_config[2]
        twilio_options[:to] = twilio_config[3]
        twilio_options
      end

      # TODO move reading of the config.yml from checker.rb to here
      # def config_options
      #   config_file = File.join(@options[:config], 'config.yml')
      #   opts = File.exist?(config_file) ? YAML.load_file(File.open(config_file)) : {}
      #   Raceweb::Utils.symbolize(opts)
      # end

      def cli_options
        cli_options = {}
        OptionParser.new do |opts|

          def help_banner
            <<~HEREDOC
              usage:
                raceweb-monitor [options]

              examples:
                show this help page:
                  $ bundle exec raceweb-monitor -h

                show the config options and exit:
                  $ bundle exec raceweb-monitor -p

                monitor app_name
                  $ bundle exec raceweb-monitor -a app_config
                  app_config must be in the form: "name1#url1#action1,action2... name2#url2#action1,action2..."

                more examples in README.md

              options:
            HEREDOC
          end

          opts.banner = help_banner

          opts.on('-a', '--app=CONFIG', 'config string for apps: "name1#url1#action1,action2..."') do |a|
            # start with any apps we have from the env_config
            cli_options[:apps] = @options[:apps] || {}
            cli_options[:apps].merge!(parse_app_configs(a))
          end
          opts.on('-t', '--twilio=TWILIO', 'config string for twilio: "SID#TOKEN#FROM#TO"') do |t|
            # start with any twillio config we have from the env_config
            cli_options[:twilio] = @options[:twillio] || {}
            cli_options[:twilio].merge!(parse_twilio_config(t))
          end
          opts.on('-s', '--sched=SCHEDULE', 'cron schedule string') do |s|
            cli_options[:schedule] = s
          end
          opts.on('-v', '--verbose=LEVEL', 'must be one of: \'sms_error\' or \'sms_full\'') do |v|
            cli_options[:verbose] = v
          end
          opts.on('-p', '--pretend', 'show the config options and exit') do |p|
            cli_options[:pretend] = p
          end
        end.parse!
        cli_options
      end

      private

      def notify(app, result=[])
        success = result[0]
        msg = result[1]
        if success
          @log.info("#{SUCCESS} #{app} #{msg}")
          sms(app, msg, SUCCESS) if %w(sms_full).include? @options[:verbose]
        else
          @log.info("#{FAIL} #{app} #{msg}")
          sms(app, msg, FAIL) if %w(sms_error sms_full).include? @options[:verbose]
        end

      end

      def sms(app, msg, emoji='')
        @twilio.api.account.messages.create(
          from: @options[:twilio][:from],
          to: @options[:twilio][:to],
          body: "#{emoji} raceweb-monitor\n#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}\n#{app} #{msg}"
        )
      end
    end
  end
end
