require 'net/http'
require 'uri'
require 'raceweb/monitor/version'
require 'pry'

module Raceweb
  module Monitor
    class Checker
      def available?(uri_str)
        uri = URI.parse(uri_str)
        short_uri = uri.host
        short_uri = uri.select(:host, :port).join(':') if uri.port != 80
        msg = "available at #{short_uri}"
        success = true
        begin
          response = Net::HTTP.get_response(uri)
          if response.code.to_i != 200
            success = false
            msg = "NOT " + msg
          end
        rescue Exception => e
          success = false
          msg = "NOT #{msg}\n#{e.message}"
        end
        return[success, msg]
      end

      def db_count?(uri_str)
        uri = URI.parse("#{uri_str}/people/count")
        msg = 'database is populated'
        success = true
        response = Net::HTTP.get_response(uri)
        if response.code.to_i != 200 || response.body.to_i == 0
          success = false
          msg = 'database is NOT populated'
        end
        return [success, msg]
      end
    end
  end
end
