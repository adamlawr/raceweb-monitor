module Raceweb
  module Utils
    def self.symbolize(obj)
      if obj.is_a? Hash
        return obj.reduce({}) do |memo, (k, v)|
          memo.tap { |m| m[k.to_sym] = symbolize(v) }
        end
      elsif obj.is_a? Array
        return obj.map { |memo| symbolize(memo) }
      end
      obj
    end
  end
end